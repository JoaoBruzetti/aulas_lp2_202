<?php
    defined('BASEPATH') or exit('No direct script access allowed');

    class  ContasModel extends CI_MODEL{

        public function __construct()
        {
            $this->load->library("conta", "", "bill");

        }
        
        public function cria($tipo){
           
            if(sizeof($_POST) == 0) return;
            
            $data = $this->input->post();
            $this->bill->cria($data);

        }

        public function lista(){
           
           $v = $this->bill->lista("pagar", 0, 0);
           die(print_r($v));

        }

    }

?>