<div style="height: 100vh">
    <div class="flex-center flex-column">

        <h3 class="h1 p-2 mb-5 bg-info text-white">Controle Financeiro Pessoal</h3>

    <form class="text-center border border-light p-5" method="POST">

        <p class="h5 p-2 mb-4 bg-info text-white">Entrar</p>
        <input type="email" id="email" name="email" class="form-control mb-4" placeholder="E-mail">
        <input type="password" id="senha" name="senha" class="form-control mb-4" placeholder="senha">
        <div class="d-flex justify-content-around"> </div>
        <button class="btn btn-info btn-block my-4" type="submit">Enviar</button>
        <p> <?= $error ? "<p class='h5 p-2 bg-danger text-white'> Dados de acesso incorretos.  </p>" : '' ?></p>

    </form>
   
    </div>
</div>